window.addEventListener('load', function () {
    const generateItemButton = document.getElementById('generateNewItem');
    generateItemButton.addEventListener('click', drawItem);
});

function drawItem() {
    let item = makeGeneratedItem();
    let fragment = generateFragment();

    getFragmentInsertDataAndAppendIt(fragment, item);
}

function generateFragment() {

    const fragment = document.createDocumentFragment();

    const div = document.createElement("div");
    div.setAttribute('id', 'item_container');
    div.style.padding = '1px';
    div.style.border = '1px solid #322a20';
    div.style.maxWidth = '355px';
    div.style.borderRadius = '2px';
    div.style.display = 'inline-block';
    div.style.boxShadow = '0 0 10px #000';
    fragment.appendChild(div);

    const divItemBody = document.createElement("div");
    divItemBody.setAttribute('id', 'item_body');
    divItemBody.style.background = "black";
    divItemBody.style.padding = "10px";
    divItemBody.style.color = "#CFB991";
    divItemBody.style.fontSize = "12px";
    div.appendChild(divItemBody);

    const divHeadTop = document.createElement("div");
    divHeadTop.setAttribute('id', 'item_top');
    divHeadTop.style.padding = "2px";
    divHeadTop.style.color = "#c7b377";
    divHeadTop.style.font = "12px, sans-serif";
    divItemBody.appendChild(divHeadTop);

    const innerDiv3 = document.createElement("div");
    innerDiv3.style.height = "40px";
    innerDiv3.style.width = "320px";
    innerDiv3.style.padding = "0 15px";
    innerDiv3.style.color = "#c7b377";
    innerDiv3.style.font = "12px, sans-serif";
    divHeadTop.appendChild(innerDiv3);

    const h3 = document.createElement("h3");
    h3.setAttribute('id', 'item_name');
    h3.style.textAlign = "center";
    h3.style.overflow = "hidden";
    h3.style.whiteSpace = "nowrap";
    h3.style.height = "40px";
    h3.style.lineHeight = "37px";
    h3.style.font = "22px, Palatino Linotype, Times, serif";
    h3.style.color = "pink";
    innerDiv3.appendChild(h3);

    const clearDiv = document.createElement("div");
    clearDiv.style.display = "block !important";
    clearDiv.style.clear = "both";
    divItemBody.appendChild(clearDiv);

    const divStats = document.createElement("div");
    divStats.setAttribute('id', 'item_stats');
    divItemBody.appendChild(divStats);

    const statsList = document.createElement("ul");
    divStats.appendChild(statsList);

    const spanItemStat = document.createElement("span");
    spanItemStat.setAttribute('id', 'item_stat');
    spanItemStat.style.textAlign = "center";
    spanItemStat.style.color = "white";
    spanItemStat.style.fontSize = "22px";
    spanItemStat.style.fontWeight = "bold";
    statsList.appendChild(spanItemStat);

    const itemArmorWeapon = document.createElement("li");
    itemArmorWeapon.setAttribute('id', 'item_armor_weapon');
    itemArmorWeapon.style.color = "#909090";
    itemArmorWeapon.style.fontSize = "12px";
    itemArmorWeapon.style.listStyle = "none";
    statsList.appendChild(itemArmorWeapon);

    const divExtras = document.createElement("div");
    divExtras.setAttribute('id', 'item_extras');
    divItemBody.appendChild(divExtras);

    const extrasList = document.createElement("ul");
    divExtras.appendChild(extrasList);

    const extrasMinLevel = document.createElement("li");
    extrasMinLevel.setAttribute('id', 'item_min_level');
    extrasMinLevel.style.margin = "1px 0";
    extrasMinLevel.innerText = "Минимальнйы уровень 70";
    extrasList.appendChild(extrasMinLevel);

    const extrasBound = document.createElement("li");
    extrasBound.setAttribute('id', 'item_extras_bound');
    extrasBound.style.margin = "1px 0";
    extrasBound.innerText = "Account bound";
    extrasList.appendChild(extrasBound);

    const divExtension = document.createElement("div");
    divExtension.setAttribute('id', 'item_extension');
    divExtension.style.margin = "10px -2px 0 -2px";
    divExtension.style.padding = "10px 12px";
    divExtension.style.borderTop = "2 px solid #322A20";
    divItemBody.appendChild(divExtension);

    const divExtensionStyles = document.createElement("div");
    divExtensionStyles.style.fontSize = "16px";
    divExtensionStyles.style.color = "#AD835A";
    divExtensionStyles.style.fontFamily = "Palatino Linotype, Times, serif";
    divExtensionStyles.style.fontStyle = "italic";
    divExtension.appendChild(divExtensionStyles);

    const extensionText = document.createElement("span");
    extensionText.setAttribute('id', 'item_extension_text');
    extensionText.style.color = "#ad835a !important";
    extensionText.innerText = "Этот мощный топор однажды уже помог...";
    divExtensionStyles.appendChild(extensionText);

    return fragment
}

function getFragmentInsertDataAndAppendIt(fragment, item) {

    fragment.querySelector("#item_name").innerText = item.itemNameAndType.name;
    fragment.querySelector("#item_name").style.color = item.itemRarityAndColor.color;
    fragment.querySelector("#item_stat").innerText = `${(item.itemStats * item.itemRarityAndColor.multiply)}`;
    fragment.querySelector("#item_armor_weapon").innerText = item.itemNameAndType.type.title;
    fragment.querySelector("#item_container").style.background = item.itemRarityAndColor.color;

    document.getElementById("main").appendChild(fragment);
}

function makeGeneratedItem() {
    return {
        itemNameAndType: generateItemNameAndType(),
        itemStats: generateItemStats(),
        itemRarityAndColor: generateItemRarityAndColor()
    }
}

function generateItemNameAndType() {

    let type = generateItemType();
    let adjective = generateItemAdjective();

    let adjectiveAndType = transformAdjectiveByNoun(type.name, adjective);
    let noun = generateItemNoun();

    return {
        name: `${adjectiveAndType} ${noun}`,
        type: type
    }
}

function transformAdjectiveByNoun(noun, adjective) {
    let transformedAdjective;
    let nounLastLetter = noun.slice(-1);

    switch (nounLastLetter) {
        case "а":
            transformedAdjective = adjective.replace(/.{2}$/gi, 'ая');
            break;
        case "ы":
        case "и":
            transformedAdjective = adjective.replace(/.{2}$/gi, 'ые');
            break;
        case "е":
        case "о":
            transformedAdjective = adjective.replace(/.{2}$/gi, 'ое');
            break;
        default:
            transformedAdjective = adjective;
            break;
    }

    return `${transformedAdjective} ${noun}`
}

function generateItemAdjective() {
    // const adjectives = ["волшебный", "мрачный", "тёмный", "чумной", "проклятый", "зачарованный", "ядовитый", "фанерный"];
    const adjectives = [
        "абсолютный",
        "адский",
        "азартный",
        "активный",
        "ангельский",
        "антагонистический",
        "арктический",
        "астрономический",
        "баснословный",
        "безапелляционный",
        "безбожный",
        "безбрежный",
        "безвозвратный",
        "безграничный",
        "бездонный",
        "бездушный",
        "безжалостный",
        "беззаветный",
        "беззастенчивый",
        "безмерный",
        "безмятежный",
        "безнадежный",
        "безоговорочный",
        "безотлагательный",
        "безраздельный",
        "безрассудный",
        "безропотный",
        "безудержный",
        "безукоризненный",
        "безумный",
        "безупречный",
        "безусловный",
        "безустанный",
        "безутешный",
        "безысходный",
        "белоснежный",
        "бескомпромиссный",
        "бесконечный",
        "беспардонный",
        "бесповоротный",
        "беспощадный",
        "беспредельный",
        "беспрекословный",
        "беспрецедентный",
        "беспримерный",
        "беспробудный",
        "беспроглядный",
        "беспросветный",
        "беспросыпный",
        "бессовестный",
        "бесстыдный",
        "бесценный",
        "бесчеловечный",
        "бесчисленный",
        "бесшабашный",
        "бешеный",
        "блестящий",
        "богатый",
        "богатырский",
        "большой",
        "буйный",
        "бурный",
        "варварский",
        "великий",
        "величайший",
        "веский",
        "весомый",
        "внушительный",
        "возмутительный",
        "волчий",
        "вопиющий",
        "восторженный",
        "волшебный",
        "впечатляющий",
        "всемерный",
        "всепоглощающий",
        "всесильный",
        "всесторонний",
        "всяческий",
        "выдающийся",
        "вылитый",
        "высокий",
        "высший",
        "галопирующий",
        "гибельный",
        "гигантский",
        "глубокий",
        "глубочайший",
        "глухой",
        "головокружительный",
        "гомерический",
        "горький",
        "горючий",
        "горячий",
        "грандиозный",
        "гробовой",
        "грозный",
        "громадный",
        "громкий",
        "громоподобный",
        "грубый",
        "густой",
        "диаметральный",
        "дивный",
        "дикий",
        "добрый",
        "доскональный",
        "дотошный",
        "дремучий",
        "душераздирающий",
        "дьявольский",
        "жаркий",
        "жгучий",
        "железный",
        "жесткий",
        "жестокий",
        "жесточайший",
        "живой",
        "животный",
        "жизненный",
        "жуткий",
        "завзятый",
        "завидный",
        "закадычный",
        "заклятый",
        "законченный",
        "закоренелый",
        "замечательно",
        "замечательный",
        "записной",
        "запредельный",
        "зачарованный",
        "заядлый",
        "звериный",
        "зверский",
        "зеленый",
        "злой",
        "злостный",
        "значительный",
        "идеальный",
        "излишний",
        "изрядный",
        "изуверский",
        "изумительный",
        "исключительный",
        "исполинский",
        "исступленный",
        "истинно",
        "истинный",
        "истовый",
        "истошный",
        "исчерпывающий",
        "каменный",
        "кардинальный",
        "катастрофический",
        "категорический",
        "клятвенный",
        "колоссальный",
        "коренной",
        "космический",
        "крайний",
        "крепкий",
        "кристальный",
        "кричащий",
        "кровный",
        "кромешный",
        "круглый",
        "крупный",
        "крутой",
        "леденящий",
        "лошадиный",
        "лютый",
        "максимальный",
        "массовый",
        "маститый",
        "матерый",
        "махровый",
        "мертвенно",
        "мертвенный",
        "мертвецкий",
        "мертвый",
        "мировой",
        "могильный",
        "могучий",
        "молниеносный",
        "мощный",
        "мрачный",
        "мучительный",
        "набитый",
        "наглый",
        "наглядный",
        "надежный",
        "надрывный",
        "наибольший",
        "наивысший",
        "нарочитый",
        "настоятельный",
        "настоящий",
        "насущный",
        "небывалый",
        "невероятный",
        "невиданный",
        "невозможный",
        "невообразимый",
        "невосполнимый",
        "невыносимый",
        "невыразимый",
        "недопустимый",
        "недосягаемый",
        "недюжинный",
        "незабываемый",
        "незаурядный",
        "неземной",
        "неизбывный",
        "неизмеримый",
        "неимоверный",
        "неиссякаемый",
        "неистовый",
        "неистощимый",
        "неистребимый",
        "неисходный",
        "неисчерпаемый",
        "неисчислимый",
        "немаловажный",
        "немалый",
        "немилосердный",
        "немой",
        "немыслимый",
        "ненасытный",
        "необоримый",
        "необузданный",
        "необыкновенный",
        "необычайный",
        "неограниченный",
        "неодолимый",
        "неописуемый",
        "неопровержимый",
        "неоспоримый",
        "неотразимый",
        "неоценимый",
        "непередаваемый",
        "непереносимый",
        "непобедимый",
        "неповторимый",
        "непогрешимый",
        "неподдельный",
        "непоколебимый",
        "непомерный",
        "непоправимый",
        "непостижимый",
        "непревзойденный",
        "непреоборимый",
        "непреодолимый",
        "непререкаемый",
        "непримиримый",
        "непробиваемый",
        "непробудный",
        "непроглядный",
        "непролазный",
        "непроходимый",
        "неразрывный",
        "нерасторжимый",
        "несгибаемый",
        "несказанный",
        "неслыханный",
        "несметный",
        "несмолкаемый",
        "несмываемый",
        "несокрушимый",
        "несравненный",
        "нестерпимый",
        "несусветный",
        "неудержимый",
        "неуемный",
        "неуклонный",
        "неукоснительный",
        "неукротимый",
        "неумеренный",
        "неустанный",
        "неусыпный",
        "неутешный",
        "неутолимый",
        "неутомимый",
        "нечеловеческий",
        "нешуточный",
        "нещадный",
        "обильный",
        "обложной",
        "образцовый",
        "оглушительный",
        "огромный",
        "ожесточенный",
        "олимпийский",
        "ослепительный",
        "ослиный",
        "основательный",
        "остервенелый",
        "острый",
        "отборный",
        "откровенный",
        "открытый",
        "отличный",
        "отменный",
        "отпетый",
        "отчаянный",
        "отъявленный",
        "ошеломляющий",
        "панический",
        "патологический",
        "первейший",
        "первоклассный",
        "первый",
        "пламенный",
        "плотный",
        "площадной",
        "повальный",
        "поголовный",
        "подавляющий",
        "подлинный",
        "подчеркнутый",
        "полнейший",
        "полный",
        "поразительный",
        "порядочный",
        "последний",
        "потрясающий",
        "предельный",
        "преклонный",
        "преступный",
        "приличный",
        "принципиальный",
        "прирожденный",
        "прожженный",
        "проклятый",
        "проливной",
        "пронзительный",
        "пронизывающий",
        "прямой",
        "пущий",
        "пьянящий",
        "рабский",
        "радикальный",
        "разгромный",
        "раздирающий",
        "разительный",
        "разящий",
        "райский",
        "ревностный",
        "революционный",
        "редкий",
        "редкостный",
        "резкий",
        "рекордный",
        "решительный",
        "рьяный",
        "сатанинский",
        "сверхчеловеческий",
        "сверхъестественный",
        "свинцовый",
        "свирепый",
        "седой",
        "сердитый",
        "серьезный",
        "сильный",
        "сказочно",
        "сказочный",
        "слепой",
        "смертельный",
        "смертный",
        "сногсшибательный",
        "собачий",
        "совершеннейший",
        "совершенный",
        "сокрушительный",
        "солидный",
        "сплошной",
        "стальной",
        "стоический",
        "стойкий",
        "стопроцентный",
        "страстный",
        "страшный",
        "строгий",
        "строжайший",
        "сумасшедший",
        "суровый",
        "существенный",
        "сущий",
        "твердый",
        "телячий",
        "тёмный",
        "титанический",
        "тотальный",
        "трескучий",
        "триумфальный",
        "тяжелый",
        "тяжкий",
        "убедительный",
        "убежденный",
        "убийственный",
        "уважительный",
        "удивительный",
        "ужасающий",
        "ужасный",
        "умопомрачительный",
        "уничтожающий",
        "фанатический",
        "фанатичный",
        "фанерный",
        "фантастический",
        "феноменальный",
        "филигранный",
        "форменный",
        "фундаментальный",
        "хороший",
        "царский",
        "цепенящий",
        "черный",
        "чертов",
        "чертовский",
        "чистый",
        "чрезвычайный",
        "чрезмерный",
        "чудовищный",
        "чумной",
        "широкий",
        "широкомасштабный",
        "шквальный",
        "штормовой",
        "шумный",
        "щедрый",
        "щемящий",
        "экстраординарный",
        "экстремальный",
        "ювелирный",
        "ядовитый",
        "ядреный",
        "яркий",
        "яростный",
        "ярый"
    ];
    return getRandomItemFromArray(adjectives);
}

function generateItemType() {
    const weaponType = ["меч", "булава", "лук", "копье", "топор", "посох", "жезл"];
    const armorType = ["шлем", "щит", "рукавицы", "сапоги", "кольцо", "пояс", "амулет", "доспехи"];

    if (Math.random() > .5) {
        return {
            type: "weapon",
            name: getRandomItemFromArray(weaponType),
            title: "урона в сек"
        }
    } else {
        return {
            type: "armor",
            name: getRandomItemFromArray(armorType),
            title: "брони"
        }
    }
}

function generateItemNoun() {
    const nouns = ["воина", "мага", "разбойника", "ангела", "демона", "шамана", "безумеца", "буйвола", "тигра", "призрака", "властелина"];
    return getRandomItemFromArray(nouns);
}


function generateItemStats() {
    return Math.floor(Math.random()*10+1)
}

function generateItemRarityAndColor() {
    const wowColors = ['#FFFFFF','#1eff00', '#0070dd', '#a335ee', '#ff8000', '#e6cc80'];
    const poeColors = ['#C8C8C8', '#8888FF', '#FFFF77', '#AF6025'];
    const diablo3Colors = ['#FFFFFF', '#6969FF', '#FFFF00', '#00FF00', '#bf642f'];

    let color = getRandomItemFromArray(wowColors);

    return {
        color:color,
        multiply: getRarityMultiply(color)
    };
}

function getRarityMultiply(color){
    let multiply = 1;
        switch (color){
            case '#9d9d9d':
            case '#C8C8C8':
            case '#FFFFFF':
                break;
            case '#1eff00':
                multiply += 5;
                break;
            case '#0070dd':
            case '#8888FF':
            case '#6969FF':
                multiply += 20;
                break;
            case '#a335ee':
            case '#FFFF77':
            case '#FFFF00':
                multiply += 300;
                break;
            case '#ff8000':
            case '#AF6025':
            case '#00FF00':
            case '#bf642f':
                multiply += 500;
                break;
            case '#e6cc80':
                multiply += 7000;
                break;
            default: multiply+=10000000;
        }

    return multiply
}

function getRandomItemFromArray(array) {
    let index = Math.floor(Math.random() * array.length);
    return array[index];
}



